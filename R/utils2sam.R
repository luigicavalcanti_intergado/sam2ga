#' Retorna fazendas conciliadas
#'
#' @param con Conexão com banco do Sistema Intergado
#'
#' @return Data frame contendo fazendas
#' @export
#'

get_fazendas_conciliadas <- function(con) {

  tryCatch({

  dbx::dbxSelect(con,
    glue::glue(
      "
        select * from ga_fazenda where intergado_fazenda_id is not null and monitorado = '1'
      "
    )
  )

  }, error = function(e) {
    futile.logger::flog.error('Erro ao buscar dados. Error: %s', e)
  })

}

#' Busca currais conciliados, opcionalmente por fazenda
#'
#' @param ga_fazenda_id Array de ids de fazenda GA
#' @param con Conexão com o banco do Sistema Intergado
#'
#' @return Data frame contendo currais conciliados
#' @export
#'
get_currais_conciliados <- function(con, ga_fazenda_id = NULL) {

  tryCatch({

    query <- glue::glue("select * from ga_curral where intergado_lote_id is not null and status = 'A'")

    if (!is.null(ga_fazenda_id)) {

      query <- paste(query, glue::glue('and fazenda_id in ({paste(ga_fazenda_id, collapse = \', \')})'))

    }

    dbx::dbxSelect(con,query)

  }, error = function(e) {
    futile.logger::flog.error('Erro ao buscar dados. Error: %s', e)
  })

}


get_int_organizacao_id <- function(con, int_fazenda_id) {

  con %>%
    dbx::dbxSelect(glue::glue('select * from int_fazenda where faz_id = {int_fazenda_id}')) %>%
    dplyr::pull(org_id)

}

bit2bol <- function(x) {
  ifelse(!is.na(x), x == 1, x)
}


buscar_gad_menos_atualizado <- function(con_si, int_fazenda_id) {

  dbx::dbxSelect(con_si, glue::glue("select gap_arquivo from int_gad_processado igp
where igp.faz_id = {int_fazenda_id} and gap_processar = true")) %>%
    dplyr::pull(gap_arquivo) %>%
    strsplit(., "\\.") %>%
    sapply(., "[[", 1) %>%
    lubridate::ymd %>%
    sort %>%
    dplyr::first

}

buscar_controle_processamento <- function(con_beef, int_curral_id, inicio, fim) {

  query <- glue::glue("select cp.curral_id, cp.dt_sinc from controle_processamento cp where cp.curral_id = {int_curral_id} and dt_sinc >= '{inicio}' and dt_sinc <= '{fim}'")

  dbx::dbxSelect(con_beef, query)

}

buscar_eventos_pesagem <- function(con_si, animais, data_ini, data_fim) {

  ani_ids <- animais %>% dplyr::pull(animal_id) %>% paste(., collapse = ',')

  query <- glue::glue(
    "select * from int_evento_pesagem where
                                  ani_id in ({ani_ids}) and eva_horario >= '{paste(data_ini, \'00:00:00\')}'  and eva_horario <= '{paste(data_fim, \'23:59:59\')}';"
  )



  tryCatch({
    dbx::dbxSelect(con_si, query)

  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar eventos de pesagem. Erro: %s', e)
  })

}

buscar_eventos_bebedouro <- function(con_si, animais, data_ini, data_fim) {

  ani_ids <- animais %>% dplyr::pull(animal_id) %>% paste(., collapse = ',')

  query <- glue::glue(
    "select * from int_evento_animal_consumo_bebedouro where
                                  ani_id in ({ani_ids}) and anc_horario_inicial >= '{paste(data_ini, \'00:00:00\')}'  and anc_horario_inicial <= '{paste(data_fim, \'23:59:59\')}';"
  )

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar eventos de bebedouro. Erro: %s', e)
  })


}

buscar_produtos_fazenda <- function(con_beef, int_fazenda_id) {

  query <- glue::glue('select * from fazenda_produto fp where fp.fazenda_id = {int_fazenda_id} ')

  dbx::dbxSelect(con_beef, query)

}

mudar_curral <- function(x){

  x %>%
    dplyr::filter(c(T, diff(curral_id) != 0)) %>%
    dplyr::rename(inicio = data) %>%
    dplyr::mutate(inicio = lubridate::ymd_hms(inicio)) -> movi

  if (nrow(movi) > 1) {
    # movi %<>% mutate(fim=lead(inicio,n = 1) - seconds(1))
    movi %>% dplyr::mutate(fim = c(inicio[-1],NA)) -> movi
  } else {
    movi$fim <- NA
  }

  return(movi)
}

add_equal_to <- function(col_name, x) {

  if (length(x) > 1) {
    eq_t <- glue::glue(' in ({paste0(x, collapse = \',\')})')
  } else {
    eq_t <- glue::glue('= {x}')
  }

  glue::glue(col_name, eq_t)
}

buscar_animais_beef <- function(con_beef, int_fazenda_id = NULL, int_curral_id = NULL, int_lote_id = NULL, int_animal_id = NULL, ativo = T, status = T) {

  query <- glue::glue('select * from animal where ativo = {ativo} and status = {status}')

  if (!is.null(int_fazenda_id)) {
    query <- glue::glue(query, add_equal_to(' and fazenda_id ', int_fazenda_id))
  }

  if (!is.null(int_curral_id)) {
    query <- glue::glue(query, add_equal_to(' and curral_id ', int_curral_id))
  }

  if (!is.null(int_lote_id)) {
    query <- glue::glue(query, add_equal_to(' and lote_id ', int_lote_id))
  }

  if (!is.null(int_animal_id)) {
    query <- glue::glue(query, add_equal_to(' and id ', int_animal_id))
  }

  tryCatch({
    dbx::dbxSelect(con_beef, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar animais. ERRROR: %s', e)
  })

}

buscar_animais_si <- function(con_si, int_fazenda_id = NULL, int_animal_id = NULL, ativo = T, status = T) {

  query <- glue::glue('select * from int_animal where ani_baixa = {!ativo} and ani_status = {status}')

  if (!is.null(int_fazenda_id)) {
    query <- glue::glue(query, add_equal_to(' and faz_id ', int_fazenda_id))
  }

  if (!is.null(int_animal_id)) {
    query <- glue::glue(query, add_equal_to(' and ani_id ', int_animal_id))
  }

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar animais. ERRROR: %s', e)
  })

}

buscar_id_ga_animal <- function(con_si, int_animal_id = NULL) {

  if (length(int_animal_id) > 1) {
    ani_id <- paste0(int_animal_id, collapse = ',')
    query <- glue::glue('select id, intergado_animal_id from ga_animal where intergado_animal_id in ({ani_id})')
  } else {
    query <- glue::glue('select id, intergado_animal_id from ga_animal where intergado_animal_id = {int_animal_id}')
  }

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar animais. ERROR: %s', e)
  })

}

buscar_id_ga_curral <- function(con_si, int_curral_id = NULL) {

  if (length(int_curral_id) > 1) {
    curral_id <- paste0(int_curral_id, collapse = ',')
    query <- glue::glue('select id, intergado_lote_id from ga_curral where id in ({curral_id})')
  } else {
    query <- glue::glue('select id, intergado_lote_id from ga_curral where id = {int_curral_id}')
  }

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar animais. ERROR: %s', e)
  })


}

buscar_id_beef_lote <- function(con_si, ga_lote_id = NULL) {

  query <- glue::glue('select id, beef_lote_id from ga_lote where {add_equal_to("id", ga_lote_id)}')

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar animais. ERROR: %s', e)
  })


}

buscar_id_ga_fazenda <- function(con_si, int_fazenda_id) {

  query <- glue::glue('select id as ga_fazenda_id, intergado_fazenda_id as int_fazenda_id from ga_fazenda gf
                      where {add_equal_to("intergado_fazenda_id", int_fazenda_id)}')

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar fazenda ERROR: %s', e)
  })

}


buscar_int_estado_animal <- function(con_si, int_animal_id) {

  query <- glue::glue('select * from int_estado_animal where {add_equal_to("ani_id", int_animal_id)} and ea_fim_atv is null')

  tryCatch({
    dbx::dbxSelect(con_si, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar estado animal. ERROR: %s', e)
  })


}

buscar_lote_animal_beef <- function(con_beef, beef_animal_id) {

  query <- glue::glue('select * from lote_animal where {add_equal_to("animal_id", beef_animal_id)} and fim is null')

  tryCatch({
    dbx::dbxSelect(con_beef, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar lote_animal. ERROR: %s', e)
  })

}

buscar_fazendas_ga <- function(con_beef) {

  query <- glue::glue('select * from fazenda_produto fp where produto_id = 2')

  tryCatch({
    dbx::dbxSelect(con_beef, query)
  }, error = function(e) {
    futile.logger::flog.error('Falha ao buscar lote_animal. ERROR: %s', e)
  })

}
