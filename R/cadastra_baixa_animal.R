#' Busca animais GA ativos não conciliados, opcionalmente por fazenda e/ou curral
#'
#' @param ga_fazenda_id ID de fazenda no banco GA
#' @param ga_curral_id Id de curral no banco GA
#' @param con_si Conexão com banco do Sistema Intergado
#'
#' @export
#'
get_animais_ga_nao_conciliados <- function(con_si, ga_fazenda_id = NULL, ga_curral_id = NULL) {
  tryCatch({

    query <- "select * from ga_animal where intergado_animal_id is null and status = 'A'"

    if (!is.null(ga_fazenda_id)) {

      query <- paste(query, glue::glue('and fazenda_id in ({paste(ga_fazenda_id, collapse = \', \')})'))

    }

    if (!is.null(ga_curral_id)) {

      query <- paste(query, glue::glue('and curral_id in ({paste(ga_curral_id, collapse = \', \')})'))

    }

    dbx::dbxSelect(con_si, query)

  }, error = function(e) {

    futile.logger::flog.error('Erro ao buscar dados. Error: %s', e)

  })
}


#' Converte dados da tabela ga_animal para formato de dados de animal da tabela int_animal
#'
#' @param animais retorno de animais da tabela ga_animal
#' @param int_fazenda_id ID de fazenda no Sistema Intergado id de fazenda na tabela int_fazenda
#' @param int_organizacao_id id de organizacao na tabela int_organizacao
#' @param keep_ani_id booleano
#'
#' @export
#'
convert_animal_ga_animal_si <- function(animais, int_fazenda_id, int_organizacao_id, keep_ani_id = F) {

  if (!keep_ani_id) {

    animais <- animais %>%
     dplyr::select(
        ani_sexo = sexo,
        ani_manejo = ncf,
        ani_tag = tag_chip,
        ani_pesoinicial = peso_entrada
      )

  } else {

    animais <- animais %>%
     dplyr::select(
        ani_id,
        ani_sexo = sexo,
        ani_manejo = ncf,
        ani_tag = tag_chip,
        ani_pesoinicial = peso_entrada
      )

  }


  animais %>%
    dplyr::mutate(
      ani_tipo_animal = 'A',
      usu_alt = 129,
      faz_id = int_fazenda_id,
      org_id = int_organizacao_id,
      ani_baixa = F,
      ani_cad_confirmado = T,
      ani_especie = 2
    )

}

#' Cadastra animal no banco SI
#'
#' @param con Conexão com banco do Sistema Intergado
#' @param animais Data frame composto dos seguintes campos: ani_id, ani_nome, ani_dt_nas, ani_tipo_animal, ani_sexo, ani_manejo, ani_tag, ani_imagem, ani_status, ani_chave_exporta, rac_id, ani_dt_inc, usu_alt, faz_id, org_id, ani_mae_id, ani_pai_id, ani_baixa, ani_cad_confirmado, ani_especie, ani_pesoinicial, ani_pesoatual, ani_dt_alt, id_animal
#'
#' @export
#'
cadastra_novo_animal_si <- function(con, animais) {

  tryCatch({

    DBI::dbBegin(con)

    con %>%
      dbx::dbxInsert(table = 'int_animal', records = animais)

    DBI::dbCommit(con)

    paste0("'", paste0(resp$ani_tag, collapse = "','"), "'") -> ani_tag_col

    con %>%
      dbx::dbxSelect(
        glue::glue(
          'select ani_id, ani_tag from int_animal
                    where (ani_tag in ({ani_tag_col})
                    and faz_id = {animais$faz_id %>% dplyr::first}
                    and ani_baixa = false)
                '
        )
      ) -> ani_id

    return(ani_id)

  }, error = function(e) {

    DBI::dbRollback(con)

    futile.logger::flog.error('Falha ao cadastrar novo animal no banco sistema intergado. ERROR: %s', e)

  })

}


#' Cadastra animal no banco BEEF
#'
#' @param con Conexão com banco Beef
#' @param animais Data frame contendo dados dos animais a serem cadastrados
#'
#' @export
#'
cadastra_animal_beef <- function(con, animais) {

  DBI::dbBegin(con)
  tryCatch({

    dbx::dbxInsert(con, 'animal', records = animais)

    DBI::dbCommit(con)
    futile.logger::flog.info("Sucesso ao cadastrar animais no beef")
  }, error = function(e) {

    DBI::dbRollback(con)
    futile.logger::flog.error("Falha ao cadastrar animais no beef. ERROR: %s", e)
    stop(e)

  })


}

#' Checa se uma tag GA está disponível para cadastro no ecossistema intergado
#'
#' @param con Conexao com o banco do Sistema Intergado
#' @param tags vetor de string de tags
#'
#' @export
#'
check_if_tag_disponivel_no_si <- function(con, tags) {

  tryCatch({

    paste0('\'',paste0(tags, collapse = '\',\''), '\'') -> tags_col

    query <- glue::glue("
      select
        a.ani_id as animal_id,
        a.ani_tag as tag,
        iea.ea_inicio_atv as inicio_atividade,
        iea.ea_fim_atv as fim_atividade,
        a.ani_cad_confirmado as cad_confirmado,
        a.faz_id as fazenda_id
      from int_animal a
      left join int_estado_animal iea on a.ani_id = iea.ani_id
      where a.ani_tag in ({tags_col}) and a.ani_baixa = false and a.ani_status = true
    ")

    tags_si <- dbx::dbxSelect(con, query)

    tibble::tibble(
      tag = tags,
    ) %>%
      dplyr::left_join(
        tags_si, by = 'tag'
      ) -> resp

    return(resp)


  }, error = function(e) {
    futile.logger::flog.error('Erro ao buscar dados. Error: %s', e)
  })

}







