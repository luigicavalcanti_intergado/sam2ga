% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/cadastra_instalacoes.R
\name{atualiza_curral_equipamentos_bd}
\alias{atualiza_curral_equipamentos_bd}
\title{Atualiza currais e equipamentos no banco beef por fazenda}
\usage{
atualiza_curral_equipamentos_bd(con_si, con_beef, int_fazenda_id)
}
\arguments{
\item{con_si}{Conexão com o banco do Sistema Intergado}

\item{con_beef}{Conexão com banco do Beef}

\item{int_fazenda_id}{ID de fazenda no Sistema Intergado}
}
\description{
Atualiza currais e equipamentos no banco beef por fazenda
}
