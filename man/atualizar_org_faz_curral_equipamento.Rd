% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/atualizar_cadastros.R
\name{atualizar_org_faz_curral_equipamento}
\alias{atualizar_org_faz_curral_equipamento}
\title{Sincronizar dados entre GA e Intergado, Clientes-organização, fazendas, currais e equipamentos}
\usage{
atualizar_org_faz_curral_equipamento(con_ga, con_si, con_beef, fazendas)
}
\arguments{
\item{con_ga}{Conexão com banco GA}

\item{con_si}{Conexão com banco do Sistema Intergado}

\item{con_beef}{Conexão com banco do Beef}

\item{fazendas}{data.frame contendo id intergado e ga de fazendas}
}
\description{
Sincronizar dados entre GA e Intergado, Clientes-organização, fazendas, currais e equipamentos
}
