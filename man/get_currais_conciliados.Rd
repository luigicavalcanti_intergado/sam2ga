% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/utils2sam.R
\name{get_currais_conciliados}
\alias{get_currais_conciliados}
\title{Busca currais conciliados, opcionalmente por fazenda}
\usage{
get_currais_conciliados(con, ga_fazenda_id = NULL)
}
\arguments{
\item{con}{Conexão com o banco do Sistema Intergado}

\item{ga_fazenda_id}{Array de ids de fazenda GA}
}
\value{
Data frame contendo currais conciliados
}
\description{
Busca currais conciliados, opcionalmente por fazenda
}
