
# Required packages -------------------------------------------------------

library(futile.logger)

library(config)
library(conflicted)

library(glue)

library(RPostgreSQL)
library(RMySQL)
library(RMariaDB)
library(dbplyr)
library(pool)
library(DBI)
library(dbx)

library(tidyr)
library(dplyr)
library(progress)

library(lubridate)
library(purrr)

conflicted::conflict_prefer("filter", "dplyr")
conflicted::conflict_prefer("select", "dplyr")

source("R/conexao_banco.R")
source("R/get_news_ga.R")
source("R/utils2sam.R")
source("R/conciliar_animais.R")
source('R/cadastra_instalacoes.R')
source('R/cadastra_baixa_animal.R')
source('R/atualizar_cadastros.R')
source('R/atualizar_eventos.R')

create_con('bd_sistema_intergado')
create_con('bd_ga')
create_con('bd_beef')

int_fazenda_id <- 170





